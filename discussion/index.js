// // EXPRESS SETUP
// // 1. Import by using the 'require' directive to get access to the components of express package/depency
// const express = require('express')
// // 2. Use the express() function and assign in to an app variable  tp create an express app or app server
// const app = express()
// // 3. Declare a variable for the port of the server
// const port = 3000

// // Middlewares
// // 4. These two .use are essential in express
// // Allows your app to reas json format data
// app.use(express.json())
// // Allows your app to read data from forms.
// app.use(express.urlencoded({extended: true}))


// // 5.You can then have your routes after setting up Express
// // Routes
// // Express has methods corresponding to each HTTP method/s
// // The full base URI for the local app for the routes will be at "http://localhost:3000"

// // Get request route
// app.get('/', (request, response) => {
// 	// once the route is accessed it will then send a string response conataining "Hello World"
// 	response.send('Hello World')
// })

// // This route expects to receive a GET request at the URI "/hello"
// app.get('/hello', (request, response) => {
// 	response.send("Hello from /hello endpoint!")
// })


// // Register user route

// // An array that will store user objects/documents when the "/register" route is accessed
// // This will also serve as our mock database
// let users = [];

// // This route expectes to receive a POST request at the URI "/register"
// // This will create a suer object in the "users" variable that mirrors a real world registration process
// app.post('/register', (request, response) => {
// 	if(request.body.username !== " " && request.body.password !== " ") {
// 		users.push(request.body)
// 		console.log(users)
// 		response.send(`User ${request.body.username} succeddfully registered`)
// 	} else {
// 		response.send('Please input BOTH username and password')
// 	}
// })


// app.put('/change-password', (request, response) => {
// 	let message

// 	for(let i = 0; i < users.length; i++) {
// 		if(request.body.username == users[i].username) {
// 			users[i].password == request.body.password
// 			message = `User ${request.body.username}'s password has been updated!`
// 			break
// 		} else {
// 			message = 'User does not exist.'
// 		}
// 	}
// 	response.send(message)
// })


// app.listen(port, () => console.log(`Server is running at port ${port}`))

const express = require('express');

const app = express();

const port = 4000;

app.use(express.json());
app.use(express.urlencoded({extended: true}));


// PART 1 /HOME
app.get('/home', (request, response) => {
	response.send('Welcome to home page.')
})

let users = [];

app.post('/register', (request, response) => {
	if(request.body.username !== " " && request.body.password !== " ") {
		users.push(request.body)
		console.log(users)
		response.send(`User ${request.body.username} succeddfully registered`)
	} else {
		response.send('Please input BOTH username and password')
	}
})

// PART 2 /USERS





// PART 3 /DELETE-USER
app.delete('/delete-user', (request, response) => {

		users.delete(request.body)
		console.log(users)
		response.send(`User ${request.body.username} successfully removed`)
})


app.listen(port, () => console.log(`Server is running at port ${port}`))